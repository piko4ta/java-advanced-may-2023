import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Scanner;

public class MaximumElement {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int numberOfCommands = Integer.parseInt(scanner.nextLine());
        ArrayDeque<Integer> elements = new ArrayDeque<Integer>();

        for (int i = 0; i < numberOfCommands; i++) {
            String[] command = scanner.nextLine().split(" ");
            switch (Integer.parseInt(command[0])) {
                case 1:
                    elements.push(Integer.parseInt(command[1]));
                    break;
                case 2:
                    elements.pop();
                    break;
                case 3:
                    System.out.println(Collections.max(elements));
                    break;

            }
        }
    }
}